
##### The CatBrowser webapp!!!!

1 To run the application, on terminal open the root folder then type "npm install" to install the dependencies.

2) Then after installing all the dependencies, type "npm start" to run the application locally.

P.S

I did not call the api request on "https://api.thecatapi.com/v1/images/EHG3sOpAM", instead I created a method that will extract the data on "https://api.thecatapi.com/v1/images/search?page=1&limit=10&breed_id=abys" result to display the cat details. I did this for optimization.

### Minor Bug
I saw a minor bug on the sample app. To reproduce I follow this steps

1) on https://grumpy.iona.dev/, select breed from the drop down, it should display the list of cats for th selected breed

2) Select a cat on the list by clicking the view details button, it should redirect you to the cat's detail page.

3) Go back to the list by clicking the Back button at the top. Notice that the url has now a search param. (ex: https://grumpy.iona.dev/?breed=abob).

4) Select another breed from the dropdown list, the url should update to "https://grumpy.iona.dev/" or to "https://grumpy.iona.dev/?breed={the new breed id selected}".

