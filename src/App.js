import './App.css';
import Home from './components/Home';
import CatDetails from './components/cats/CatDetails';
import Cat from './components/cats/Cat';
import { Route, Routes } from 'react-router-dom';

function App() {
  return (
    <Routes>
      <Route path="/" element={<Home />} />
      <Route path=":id" element={<CatDetails />} />
    </Routes>
  );
}

export default App;
