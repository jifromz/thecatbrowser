
export const BREEDS_URL = 'https://api.thecatapi.com/v1/breeds';

export const filterBreedsByName = (limit = 10, currPage = 1 , breed_id) => {
    return `https://api.thecatapi.com/v1/images/search?page=${currPage}&limit=${limit}&breed_id=${breed_id}`;
}


