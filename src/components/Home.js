import React, { useEffect, useReducer, createContext } from "react";
import '../App.css';
import { BREEDS_URL } from '../data/api';
import axios from "axios";
import { breedsReducer } from '../reducers/breedsReducer';
import {
    FETCH_BREEDS_SUCCESS,
    FETCH_BREEDS_FAILED,
    FILTER_BREEDS_SUCCESS,
    FILTER_BREEDS_FAILED,
    FETCHING,
    FETCHING_DONE,
    UPDATE_BREED_ID
} from '../actions';
import DropDownSelect from "./_base/DropDownSelect";
import Cats from "./cats/Cats";
import Button from "./_base/Button";
import { COL_STYLE } from '../styleHelper';

import { filterBreedsByName } from '../data/api';
import { Outlet, useSearchParams } from "react-router-dom";

export const AppContext = createContext(null);
const initialState = { breeds: [], filteredBreeds: [], breedId: null, currPage: 1, isFetching: false }

const Home = () => {

    const [searchParams, setSearchParams] = useSearchParams();
    const searchTerm = searchParams.get('breed') || '';
    const [state, dispatch] = useReducer(breedsReducer, initialState);

    const limit = 10;

    useEffect(() => {

        dispatch({ type: FETCHING });
        axios.get(BREEDS_URL).then(res => {
            dispatch({ type: FETCH_BREEDS_SUCCESS, breeds: res.data });
            dispatch({ type: FETCHING_DONE });
        }).catch(error => {
            dispatch({ type: FETCH_BREEDS_FAILED, breeds: null });
            dispatch({ type: FETCHING_DONE });
        });

        console.log(searchTerm)

        if (searchTerm) {
            dispatch({ type: UPDATE_BREED_ID, breedId: searchTerm })
            queryFilteredBreeds(searchTerm, 1);
        }

    }, []);


    const queryFilteredBreeds = async (breedId, page) => {
        await axios.get(filterBreedsByName(limit, page, breedId)).then(res => {
            dispatch({ type: FILTER_BREEDS_SUCCESS, filteredBreeds: res.data });
            dispatch({ type: FETCHING_DONE });
        }).catch(err => {
            dispatch({ type: FILTER_BREEDS_FAILED, filteredBreeds: [] });
            dispatch({ type: FETCHING_DONE });
        });
    }


    return (
        <AppContext.Provider value={{
            state,
            dispatch,
            queryFilteredBreeds,
            setSearchParams,
            searchTerm
        }}>
            <div className="Home">
                <div className="container">
                    <h1>Cat Browser</h1>
                    <div className="row" style={{ paddingBottom: '10px', paddingTop: '10px' }}>
                        <div className={COL_STYLE}>
                            {
                                <DropDownSelect />
                            }
                        </div>
                    </div>
                    <div className="row">
                        <Cats />
                    </div>
                    <div className="row" >
                        <div className={COL_STYLE}>
                            <Button />
                        </div>
                    </div>
                </div>
            </div>
        </AppContext.Provider>
    )
}

export default Home;