import React, { useEffect, useContext, useParams } from "react";
import { useLocation, useNavigate } from 'react-router-dom';
import '../../App.css';
import { AppContext } from '../Home';

const CatDetails = () => {

    const location = useLocation();
    const navigate = useNavigate()
    const { catDetails, breed_Id } = location.state;
    const { temperament, origin, description, url, name } = catDetails;
    
    useEffect(() => {
        console.log(breed_Id);
    }, []);

    const onClick = (e) => {
        e.preventDefault();
        // setSearchParams({ breed: breed_Id});
        navigate(`/?breed=${breed_Id}`);
    }

    return (
        <div className="Cat">
            <div className="container">
                <div className="card">
                    <div className="card-header">
                        <button type="button"
                            className="btn btn-primary"
                            onClick={(e) => onClick(e)}
                        >
                            Back
                        </button>
                    </div>
                    <img src={url} className="card-img" alt="..." />
                    <div className="card-body">
                        <h4>{name}</h4>
                        <h5>{`Origin: ${origin}`}</h5>
                        <h6>{temperament}</h6>
                        <p>{description}</p>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default CatDetails;