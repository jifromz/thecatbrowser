import React, { useEffect } from "react";
import { useLocation, useNavigate } from 'react-router-dom';
import '../../App.css';
import Cats from "./Cats";
import DropDownSelect from "../_base/DropDownSelect";
import Button from "../_base/Button";
import { COL_STYLE } from '../../styleHelper';

const Cat = () => {

    useEffect(() => {

    }, []);

    return (
       
            <div className="container">
                <h1>Cat Browser</h1>
                <div className="row" style={{ paddingBottom: '10px', paddingTop: '10px' }}>
                    <div className={COL_STYLE}>
                        {
                            <DropDownSelect />
                        }
                    </div>
                </div>
                <div className="row">
                    <Cats />
                </div>
                <div className="row" >
                    <div className={COL_STYLE}>
                        <Button />
                    </div>
                </div>
            </div>
     
    )
}

export default Cat;