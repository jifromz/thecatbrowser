import React, { useContext, useEffect } from "react";
import { COL_STYLE } from '../../styleHelper';
import { Link } from "react-router-dom";
import { AppContext } from '../Home';
 
 
const Cats = () => {

    const { state } = useContext(AppContext);
    const { filteredBreeds, isFetching, breedId } = state;

    useEffect(() => {

    }, [])

    return (
        <>
            {
                isFetching || !filteredBreeds ?
                    <div className="col-12" style={{ marginBottom: "20px" }}>No cats available</div> :
                    filteredBreeds.map(cat => {
                        return (
                            <div key={cat.imageId} className={COL_STYLE}>
                                <div className="card">
                                    <img src={cat.url} className="card-img-top" alt="..." />
                                    <div className="card-body">
                                        <Link className="btn btn-primary btn-block"
                                            to={`/${cat.imageId}`}
                                            state={{ catDetails: cat, breed_Id: breedId }}
                                        >
                                            View details
                                        </Link>
                                    </div>
                                </div>
                            </div>
                        )
                    })
            }
        </>
    )
}

export default React.memo(Cats); 