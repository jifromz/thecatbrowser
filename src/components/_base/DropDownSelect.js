import React, { useContext } from 'react';
import { AppContext } from '../Home';
import { UPDATE_BREED_ID, INCREMENT_PAGE, FETCHING, CLEAR_FILTERED_LIST } from '../../actions';

const DropDownSelect = () => {
    const { state, dispatch, queryFilteredBreeds } = useContext(AppContext);
    const { breeds, isFetching, breedId } = state;
    const { setSearchParams, searchTerm } = useContext(AppContext);

    const onSelectBreedId = (e) => {
        const { value } = e.target;
        dispatch({ type: FETCHING });
        dispatch({ type: UPDATE_BREED_ID, breedId: value });
        dispatch({ type: INCREMENT_PAGE, page: 1 });
        dispatch({ type: CLEAR_FILTERED_LIST });
        setSearchParams({});
        queryFilteredBreeds(value, 1);
    }

    return (
        <form>
            <div className="mb-3">
                <label htmlFor="breed" className="form-label">Breed</label>
                <select value={breedId || ""} className="form-select"
                    aria-label="Default select example"
                    disabled={isFetching}
                    onChange={(e) => onSelectBreedId(e)}
                >
                    <option>Select breed</option>
                    {breeds.map(({ name, id }) => {
                        return <option key={id} value={id}>{name}</option>
                    })}
                </select>
            </div>
        </form>
    )
}

export default DropDownSelect;