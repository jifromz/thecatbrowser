import React, { useContext, useEffect } from "react";
import { AppContext } from '../Home';
import { INCREMENT_PAGE, FETCHING } from '../../actions';

const Button = () => {
    const { state, dispatch, queryFilteredBreeds } = useContext(AppContext);
    const { isFetching, breedId, currPage } = state;

    const loadMoreCats = (e) => {
        let page = currPage + 1;
        dispatch({ type: FETCHING });
        dispatch({ type: INCREMENT_PAGE, page: page });
        queryFilteredBreeds(state.breedId, page);
    }

    useEffect(() => {

    }, []);

    return (
        <button
            type="button"
            className="btn btn-success"
            disabled={isFetching || !breedId}
            onClick={loadMoreCats}
        >
            {isFetching && breedId ? "Loading cats..." : "Load more"}
        </button>
    )
}

export default Button;