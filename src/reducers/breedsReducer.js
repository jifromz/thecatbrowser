import {
    FETCH_BREEDS_FAILED,
    FETCH_BREEDS_SUCCESS,
    FETCHING,
    INCREMENT_PAGE,
    FILTER_BREEDS_SUCCESS,
    FILTER_BREEDS_FAILED,
    FETCHING_DONE,
    UPDATE_BREED_ID,
    CLEAR_FILTERED_LIST
} from '../actions';

export const breedsReducer = (state, action) => {
    let newState;
    switch (action.type) {
        case FETCH_BREEDS_SUCCESS:
            newState = { ...state, breeds: destructureBreeds(action.breeds) }
            break;
        case FETCH_BREEDS_FAILED:
            newState = { ...state, breeds: null }
            break;
        case FILTER_BREEDS_SUCCESS:
            newState = { ...state, filteredBreeds: returnUnique(state.filteredBreeds, destructureFilteredCats(action.filteredBreeds)) }
            break;
        case FILTER_BREEDS_FAILED:
            newState = { ...state, filteredBreeds: [] }
            break;
        case CLEAR_FILTERED_LIST:
            newState = { ...state, filteredBreeds: [] }
            break;
        case INCREMENT_PAGE:
            newState = { ...state, currPage: action.page }
            break;
        case FETCHING:
            newState = { ...state, isFetching: true }
            break;
        case FETCHING_DONE:
            newState = { ...state, isFetching: false }
            break;
        case UPDATE_BREED_ID:
            newState = { ...state, breedId: action.breedId }
            break;
        default:
            throw new Error();
    }
    return newState;
}

const destructureBreeds = (breeds) => {
    if (!breeds) return [];
    return breeds.map(({ id, name }) => {
        return { id, name }
    });
}

const destructureFilteredCats = (filteredBreeds) => {
    if (!filteredBreeds) return;
    return filteredBreeds.map(breed => {
        const { id, temperament, origin, description, name } = breed.breeds[0];
        const imageId = breed.id;
        const { width, height, url } = breed;
        return { id, imageId, temperament, origin, description, name, width, height, url };
    });
}

const returnUnique = (currArr, newArr) => {
    if (!currArr) return newArr;
    let newList = [];
    newList = currArr;
    newArr.forEach(item => {
        const newItem = newList.find(oldItem => oldItem.imageId.toString() === item.imageId.toString())
        if (!newItem) newList.push(item);
    });
    return newList;
}


